﻿using System;
using System.Windows.Forms;

namespace bdm_v1r2_241
{
    public partial class frmLaunch : Form
    {
        public frmLaunch()
        {
            InitializeComponent();
        }

        private void frmLaunch_Shown(object sender, EventArgs e)
        {
        }

        private void Passwords_Click(object sender, EventArgs e)
        {
            var frm = new frmSubjectofPasswords();
            frm.Show();
        }

        private void Keypads_Click(object sender, EventArgs e)
        {
            var frm = new frmSubjectofKeypads();
            frm.Show();
        }

        private void btnSubjectofWires_Click(object sender, EventArgs e)
        {
            var frm = new frmSubjectOfWires();
            frm.Show();
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            var frm = new frmAbout();
            frm.ShowDialog();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void TheButton_Click(object sender, EventArgs e)
        {
            var frm = new frmSubjectofTheButton();
            frm.Show();
        }

        private void btnSubjectOfWireSequences_Click(object sender, EventArgs e)
        {
            var frm = new frmSubjectofWireSequences();
            frm.Show();
        }

        private void btnSubjectofMemory_Click(object sender, EventArgs e)
        {
            var frm = new frmSubjectofMemory();
            frm.Show();
        }
    }
}