## Keep Talking and Nobody Explodes 
### bomb manual assist version 1, release 2 241
by John Reynolds

Built to assist with the wonderfully fun game, [*Keep Talking and Nobody Explodes*](http://www.keeptalkinggame.com)

I've currently only completed about five modules - not all of them really need any assistance.
Next up will be subject of memory.

If you'd like to commit or assist, please let me know.