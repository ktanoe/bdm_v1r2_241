﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace bdm_v1r2_241
{
    public partial class frmSubjectofKeypads : Form
    {
        public frmSubjectofKeypads()
        {
            InitializeComponent();

            _columnLists = new List<SortedList<int, string>>
            {
                _col1List,
                _col2List,
                _col3List,
                _col4List,
                _col5List,
                _col6List
            };

            _keyPressedList = new SortedList<int, string>();

            _keyPressCounter = 1;
        }

        private void frmSubjectofKeypads_Load(object sender, EventArgs e)
        {
            foreach (var btn in tableLayoutPanel1.Controls.Cast<Button>())
            {
                btn.Click += ClickedAButton;
            }
        }

        private void ClickedAButton(object sender, EventArgs e)
        {
            if (_keyPressCounter > 4)
            {
                ResetAll();
                return;
            }

            var btnPressed = (Button) sender;

            _keyPressedList.Add(_keyPressCounter, btnPressed.Text);

            btnPressed.Enabled = false;
            btnPressed.BackColor = Color.LightYellow;

            lblSelected.Text = "Selected:  " + string.Join(", ", _keyPressedList.ToArray());

            _keyPressCounter += 1;

            if (_keyPressedList.Count != 4) return;

            foreach (var col in _columnLists)
            {
                if (_keyPressedList.All(s => col.ContainsValue(s.Value)))
                {
                    var output = new SortedList<int, string>();
                    var counter = 1;
                    foreach (var colPair in col.Where(colPair => _keyPressedList.ContainsValue(colPair.Value)))
                    {
                        output.Add(counter, colPair.Value);
                        counter += 1;
                    }

                    lblResults.Text = "Results:  " + string.Join(", ", output.ToArray());

                    ButtonsDisableAll();

                    break;
                }
            }
        }

        private void ButtonsDisableAll()
        {
            foreach (var btn in tableLayoutPanel1.Controls.Cast<Button>())
            {
                btn.Enabled = false;
            }
        }

        private void ButtonsReset()
        {
            foreach (var btn in tableLayoutPanel1.Controls.Cast<Button>())
            {
                btn.Enabled = true;
                btn.BackColor = Color.Transparent;
            }
        }

        private void ResetAll()
        {
            ButtonsReset();
            lblSelected.Text = "Selected: ";
            lblResults.Text = "Ordered: ";
            _keyPressedList = new SortedList<int, string>();
            _keyPressCounter = 1;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ResetAll();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tslblTimer.Text = $@"{DateTime.Now.Subtract(_openDateTime):dd\.hh\:mm\:ss}";
        }

        #region "Variables"

        private readonly SortedList<int, string> _col1List = new SortedList<int, string>
        {
            {1, "Tennis raquet"},
            {2, "Letter A"},
            {3, "Lambda"},
            {4, "Lightning bolt"},
            {5, "Cat"},
            {6, "Letter H"},
            {7, "C backwards"}
        };

        private readonly SortedList<int, string> _col2List = new SortedList<int, string>
        {
            {1, "C umlaut"},
            {2, "Tennis raquet"},
            {3, "C backwards"},
            {4, "Whisk"},
            {5, "Star - open"},
            {6, "Letter H"},
            {7, "Question mark"}
        };

        private readonly SortedList<int, string> _col3List = new SortedList<int, string>
        {
            {1, "Copyright"},
            {2, "Nose"},
            {3, "Whisk"},
            {4, "Psi - mirrored"},
            {5, "Letter R"},
            {6, "Lambda"},
            {7, "Star - open"}
        };

        private readonly SortedList<int, string> _col4List = new SortedList<int, string>
        {
            {1, "6"},
            {2, "Paragraph"},
            {3, "Letter B"},
            {4, "Cat"},
            {5, "Psi - mirrored"},
            {6, "Question mark"},
            {7, "Smiley face"}
        };

        private readonly SortedList<int, string> _col5List = new SortedList<int, string>
        {
            {1, "Psi"},
            {2, "Smiley face"},
            {3, "Letter B"},
            {4, "Letter C"},
            {5, "Paragraph"},
            {6, "Mouse / 3"},
            {7, "Star - filled"}
        };

        private readonly SortedList<int, string> _col6List = new SortedList<int, string>
        {
            {1, "6"},
            {2, "C umlaut"},
            {3, "Railroad track"},
            {4, "ae"},
            {5, "Psi"},
            {6, "Letter H"},
            {7, "Omega"}
        };

        private readonly List<SortedList<int, string>> _columnLists;
        private SortedList<int, string> _keyPressedList;
        private int _keyPressCounter;

        private readonly DateTime _openDateTime = DateTime.Now;

        #endregion
    }
}