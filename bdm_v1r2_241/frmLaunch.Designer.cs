﻿namespace bdm_v1r2_241
{
    partial class frmLaunch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSubjectofButton = new System.Windows.Forms.Button();
            this.btnSubjectofKeypads = new System.Windows.Forms.Button();
            this.btnSubjectofMemory = new System.Windows.Forms.Button();
            this.btnSubjectofPasswords = new System.Windows.Forms.Button();
            this.btnSubjectOfWireSequences = new System.Windows.Forms.Button();
            this.btnSubjectofWires = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.timerClock = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnSubjectofButton);
            this.flowLayoutPanel1.Controls.Add(this.btnSubjectofKeypads);
            this.flowLayoutPanel1.Controls.Add(this.btnSubjectofMemory);
            this.flowLayoutPanel1.Controls.Add(this.btnSubjectofPasswords);
            this.flowLayoutPanel1.Controls.Add(this.btnSubjectOfWireSequences);
            this.flowLayoutPanel1.Controls.Add(this.btnSubjectofWires);
            this.flowLayoutPanel1.Controls.Add(this.btnAbout);
            this.flowLayoutPanel1.Controls.Add(this.btnExit);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(543, 228);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnSubjectofButton
            // 
            this.btnSubjectofButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSubjectofButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubjectofButton.Image = global::bdm_v1r2_241.Properties.Resources.ic_file_download_black_24dp_2x;
            this.btnSubjectofButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSubjectofButton.Location = new System.Drawing.Point(3, 3);
            this.btnSubjectofButton.Name = "btnSubjectofButton";
            this.btnSubjectofButton.Size = new System.Drawing.Size(175, 70);
            this.btnSubjectofButton.TabIndex = 0;
            this.btnSubjectofButton.TabStop = false;
            this.btnSubjectofButton.Text = "On the subject of The Button";
            this.btnSubjectofButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSubjectofButton.UseVisualStyleBackColor = true;
            this.btnSubjectofButton.Click += new System.EventHandler(this.TheButton_Click);
            // 
            // btnSubjectofKeypads
            // 
            this.btnSubjectofKeypads.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSubjectofKeypads.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubjectofKeypads.Image = global::bdm_v1r2_241.Properties.Resources.ic_dialpad_black_24dp;
            this.btnSubjectofKeypads.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSubjectofKeypads.Location = new System.Drawing.Point(184, 3);
            this.btnSubjectofKeypads.Name = "btnSubjectofKeypads";
            this.btnSubjectofKeypads.Size = new System.Drawing.Size(175, 70);
            this.btnSubjectofKeypads.TabIndex = 1;
            this.btnSubjectofKeypads.TabStop = false;
            this.btnSubjectofKeypads.Text = "On the subject of Keypads";
            this.btnSubjectofKeypads.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSubjectofKeypads.UseVisualStyleBackColor = true;
            this.btnSubjectofKeypads.Click += new System.EventHandler(this.Keypads_Click);
            // 
            // btnSubjectofMemory
            // 
            this.btnSubjectofMemory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSubjectofMemory.Enabled = false;
            this.btnSubjectofMemory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubjectofMemory.Image = global::bdm_v1r2_241.Properties.Resources.ic_face_black_24dp_2x;
            this.btnSubjectofMemory.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSubjectofMemory.Location = new System.Drawing.Point(365, 3);
            this.btnSubjectofMemory.Name = "btnSubjectofMemory";
            this.btnSubjectofMemory.Size = new System.Drawing.Size(175, 70);
            this.btnSubjectofMemory.TabIndex = 7;
            this.btnSubjectofMemory.TabStop = false;
            this.btnSubjectofMemory.Text = "On the subject of Memory";
            this.btnSubjectofMemory.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSubjectofMemory.UseVisualStyleBackColor = true;
            this.btnSubjectofMemory.Click += new System.EventHandler(this.btnSubjectofMemory_Click);
            // 
            // btnSubjectofPasswords
            // 
            this.btnSubjectofPasswords.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSubjectofPasswords.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubjectofPasswords.Image = global::bdm_v1r2_241.Properties.Resources.ic_lock_black_24dp;
            this.btnSubjectofPasswords.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSubjectofPasswords.Location = new System.Drawing.Point(3, 79);
            this.btnSubjectofPasswords.Name = "btnSubjectofPasswords";
            this.btnSubjectofPasswords.Size = new System.Drawing.Size(175, 70);
            this.btnSubjectofPasswords.TabIndex = 2;
            this.btnSubjectofPasswords.TabStop = false;
            this.btnSubjectofPasswords.Text = "On the subject of Passwords";
            this.btnSubjectofPasswords.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSubjectofPasswords.UseVisualStyleBackColor = true;
            this.btnSubjectofPasswords.Click += new System.EventHandler(this.Passwords_Click);
            // 
            // btnSubjectOfWireSequences
            // 
            this.btnSubjectOfWireSequences.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSubjectOfWireSequences.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubjectOfWireSequences.Image = global::bdm_v1r2_241.Properties.Resources.ic_share_black_24dp_2x;
            this.btnSubjectOfWireSequences.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSubjectOfWireSequences.Location = new System.Drawing.Point(184, 79);
            this.btnSubjectOfWireSequences.Name = "btnSubjectOfWireSequences";
            this.btnSubjectOfWireSequences.Size = new System.Drawing.Size(175, 70);
            this.btnSubjectOfWireSequences.TabIndex = 6;
            this.btnSubjectOfWireSequences.TabStop = false;
            this.btnSubjectOfWireSequences.Text = "On the Subject of Wire Sequences";
            this.btnSubjectOfWireSequences.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSubjectOfWireSequences.UseVisualStyleBackColor = true;
            this.btnSubjectOfWireSequences.Click += new System.EventHandler(this.btnSubjectOfWireSequences_Click);
            // 
            // btnSubjectofWires
            // 
            this.btnSubjectofWires.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSubjectofWires.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubjectofWires.Image = global::bdm_v1r2_241.Properties.Resources.ic_swap_calls_black_24dp_2x;
            this.btnSubjectofWires.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSubjectofWires.Location = new System.Drawing.Point(365, 79);
            this.btnSubjectofWires.Name = "btnSubjectofWires";
            this.btnSubjectofWires.Size = new System.Drawing.Size(175, 70);
            this.btnSubjectofWires.TabIndex = 3;
            this.btnSubjectofWires.TabStop = false;
            this.btnSubjectofWires.Text = "On the subject of Wires";
            this.btnSubjectofWires.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSubjectofWires.UseVisualStyleBackColor = true;
            this.btnSubjectofWires.Click += new System.EventHandler(this.btnSubjectofWires_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAbout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbout.Image = global::bdm_v1r2_241.Properties.Resources.ic_help_outline_black_24dp_2x;
            this.btnAbout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbout.Location = new System.Drawing.Point(3, 155);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(175, 70);
            this.btnAbout.TabIndex = 4;
            this.btnAbout.TabStop = false;
            this.btnAbout.Text = "About";
            this.btnAbout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = global::bdm_v1r2_241.Properties.Resources.ic_close_black_24dp_2x;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(184, 155);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(175, 70);
            this.btnExit.TabIndex = 5;
            this.btnExit.TabStop = false;
            this.btnExit.Text = "Exit";
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // timerClock
            // 
            this.timerClock.Interval = 500;
            // 
            // frmLaunch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(575, 228);
            this.Controls.Add(this.flowLayoutPanel1);
            this.MaximizeBox = false;
            this.Name = "frmLaunch";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bomb manual v1 r2 241";
            this.Shown += new System.EventHandler(this.frmLaunch_Shown);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnSubjectofPasswords;
        private System.Windows.Forms.Timer timerClock;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.Button btnSubjectofKeypads;
        private System.Windows.Forms.Button btnSubjectofWires;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSubjectofButton;
        private System.Windows.Forms.Button btnSubjectOfWireSequences;
        private System.Windows.Forms.Button btnSubjectofMemory;
    }
}