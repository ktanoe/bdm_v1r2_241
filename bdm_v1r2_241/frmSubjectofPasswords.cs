﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace bdm_v1r2_241
{
    public partial class frmSubjectofPasswords : Form
    {
        #region "Variables"

        private readonly List<string> _pwdList = new List<string>
        {
            "about",
            "after",
            "again",
            "below",
            "could",
            "every",
            "first",
            "found",
            "great",
            "house",
            "large",
            "learn",
            "never",
            "other",
            "place",
            "plant",
            "point",
            "right",
            "small",
            "sound",
            "spell",
            "still",
            "study",
            "their",
            "there",
            "these",
            "thing",
            "think",
            "three",
            "water",
            "where",
            "which",
            "world",
            "would",
            "write"
        };

        private readonly DateTime _openDateTime = DateTime.Now;

        #endregion

        public frmSubjectofPasswords()
        {
            InitializeComponent();
        }

        private void frmPassword_Load(object sender, EventArgs e)
        {
            ClearAndReset();
        }

        private void ClearAndReset()
        {
            Passwords.Text = _pwdList.Aggregate(string.Empty, (current, s) => current + s + " ");
            Col1.Text = string.Empty;
            Col2.Text = string.Empty;
            Col3.Text = string.Empty;
            Col4.Text = string.Empty;
            Col5.Text = string.Empty;
            Col1.Focus();
        }

        private void CalcPass(object sender, EventArgs e)
        {
            List<string> remainder;

            remainder = FindWord(_pwdList, Col1.Text.Trim(), 1);
            remainder = FindWord(remainder, Col2.Text.Trim(), 2);
            remainder = FindWord(remainder, Col3.Text.Trim(), 3);
            remainder = FindWord(remainder, Col4.Text.Trim(), 4);
            remainder = FindWord(remainder, Col5.Text.Trim(), 5);

            if (remainder.Count > 0)
            {
                Passwords.Text = remainder.Aggregate(string.Empty, (current, s) => current + s + " ");
            }
            else
            {
                Passwords.Text = _pwdList.Aggregate(string.Empty, (current, s) => current + s + " ");
            }
        }

        private List<string> FindWord(List<string> myList, string typedText, int colNumber)
        {
            // Nothing typed yet
            if (string.IsNullOrEmpty(typedText))
            {
                return myList;
            }

            var found = new List<string>();

            foreach (var letter in typedText)
            {
                List<string> retval;

                if (colNumber == 1)
                {
                    // Find where starts with
                    retval = myList.Where(x => x.StartsWith(letter.ToString())).ToList();
                }
                else
                {
                    // Find substring of column # (zero length so we subtract one)
                    retval = myList.Where(x => x.Substring(colNumber - 1, 1) == letter.ToString()).ToList();
                }

                found.AddRange(retval);
            }

            //note that if we found nothing, return what we started with, otherwise what we found
            return found.Count == 0 ? myList : found.Distinct().ToList();
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            ClearAndReset();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tslblTimer.Text = $@"{DateTime.Now.Subtract(_openDateTime):dd\.hh\:mm\:ss}";
        }
    }
}