﻿namespace bdm_v1r2_241
{
    partial class frmSubjectofTheButton
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnYellow = new System.Windows.Forms.Button();
            this.btnWhite = new System.Windows.Forms.Button();
            this.btnBlue = new System.Windows.Forms.Button();
            this.btnRed = new System.Windows.Forms.Button();
            this.btnOther = new System.Windows.Forms.Button();
            this.btnHold = new System.Windows.Forms.Button();
            this.btnDetonate = new System.Windows.Forms.Button();
            this.btnAbort = new System.Windows.Forms.Button();
            this.txtResults = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnLabelNone = new System.Windows.Forms.Button();
            this.grpColor = new System.Windows.Forms.GroupBox();
            this.grpLabel = new System.Windows.Forms.GroupBox();
            this.grpIndicator = new System.Windows.Forms.GroupBox();
            this.btnIndicatorNone = new System.Windows.Forms.Button();
            this.btnFRK = new System.Windows.Forms.Button();
            this.btnCar = new System.Windows.Forms.Button();
            this.txtSelected = new System.Windows.Forms.TextBox();
            this.grpSelected = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslblTimer = new System.Windows.Forms.ToolStripStatusLabel();
            this.grpBattery = new System.Windows.Forms.GroupBox();
            this.btnBattery1 = new System.Windows.Forms.Button();
            this.btnBattery2 = new System.Windows.Forms.Button();
            this.btnBattery3 = new System.Windows.Forms.Button();
            this.btnBattery4 = new System.Windows.Forms.Button();
            this.btnBatteryNone = new System.Windows.Forms.Button();
            this.grpColor.SuspendLayout();
            this.grpLabel.SuspendLayout();
            this.grpIndicator.SuspendLayout();
            this.grpSelected.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.grpBattery.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(9, 550);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(100, 45);
            this.btnReset.TabIndex = 0;
            this.btnReset.TabStop = false;
            this.btnReset.Text = "R&eset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnYellow
            // 
            this.btnYellow.BackColor = System.Drawing.Color.Yellow;
            this.btnYellow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnYellow.Location = new System.Drawing.Point(6, 149);
            this.btnYellow.Name = "btnYellow";
            this.btnYellow.Size = new System.Drawing.Size(124, 58);
            this.btnYellow.TabIndex = 2;
            this.btnYellow.Text = "YELLOW";
            this.btnYellow.UseVisualStyleBackColor = false;
            // 
            // btnWhite
            // 
            this.btnWhite.BackColor = System.Drawing.Color.White;
            this.btnWhite.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWhite.Location = new System.Drawing.Point(6, 213);
            this.btnWhite.Name = "btnWhite";
            this.btnWhite.Size = new System.Drawing.Size(124, 58);
            this.btnWhite.TabIndex = 3;
            this.btnWhite.Text = "WHITE";
            this.btnWhite.UseVisualStyleBackColor = false;
            // 
            // btnBlue
            // 
            this.btnBlue.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnBlue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBlue.Location = new System.Drawing.Point(6, 85);
            this.btnBlue.Name = "btnBlue";
            this.btnBlue.Size = new System.Drawing.Size(124, 58);
            this.btnBlue.TabIndex = 1;
            this.btnBlue.Text = "BLUE";
            this.btnBlue.UseVisualStyleBackColor = false;
            // 
            // btnRed
            // 
            this.btnRed.BackColor = System.Drawing.Color.Red;
            this.btnRed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRed.Location = new System.Drawing.Point(6, 21);
            this.btnRed.Name = "btnRed";
            this.btnRed.Size = new System.Drawing.Size(124, 58);
            this.btnRed.TabIndex = 0;
            this.btnRed.Text = "RED";
            this.btnRed.UseVisualStyleBackColor = false;
            // 
            // btnOther
            // 
            this.btnOther.BackColor = System.Drawing.Color.Transparent;
            this.btnOther.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOther.Location = new System.Drawing.Point(6, 277);
            this.btnOther.Name = "btnOther";
            this.btnOther.Size = new System.Drawing.Size(124, 58);
            this.btnOther.TabIndex = 4;
            this.btnOther.Text = "OTHER";
            this.btnOther.UseVisualStyleBackColor = false;
            // 
            // btnHold
            // 
            this.btnHold.BackColor = System.Drawing.Color.Transparent;
            this.btnHold.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHold.Location = new System.Drawing.Point(6, 21);
            this.btnHold.Name = "btnHold";
            this.btnHold.Size = new System.Drawing.Size(124, 58);
            this.btnHold.TabIndex = 0;
            this.btnHold.Text = "Hold";
            this.btnHold.UseVisualStyleBackColor = false;
            // 
            // btnDetonate
            // 
            this.btnDetonate.BackColor = System.Drawing.Color.Transparent;
            this.btnDetonate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDetonate.Location = new System.Drawing.Point(6, 149);
            this.btnDetonate.Name = "btnDetonate";
            this.btnDetonate.Size = new System.Drawing.Size(124, 58);
            this.btnDetonate.TabIndex = 2;
            this.btnDetonate.Text = "Detonate";
            this.btnDetonate.UseVisualStyleBackColor = false;
            // 
            // btnAbort
            // 
            this.btnAbort.BackColor = System.Drawing.Color.Transparent;
            this.btnAbort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbort.Location = new System.Drawing.Point(6, 85);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(124, 58);
            this.btnAbort.TabIndex = 1;
            this.btnAbort.Text = "Abort";
            this.btnAbort.UseVisualStyleBackColor = false;
            // 
            // txtResults
            // 
            this.txtResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResults.Location = new System.Drawing.Point(9, 427);
            this.txtResults.Multiline = true;
            this.txtResults.Name = "txtResults";
            this.txtResults.ReadOnly = true;
            this.txtResults.Size = new System.Drawing.Size(709, 103);
            this.txtResults.TabIndex = 24;
            this.txtResults.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 408);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 25;
            this.label4.Text = "Results";
            // 
            // btnLabelNone
            // 
            this.btnLabelNone.BackColor = System.Drawing.Color.Transparent;
            this.btnLabelNone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLabelNone.Location = new System.Drawing.Point(6, 213);
            this.btnLabelNone.Name = "btnLabelNone";
            this.btnLabelNone.Size = new System.Drawing.Size(124, 58);
            this.btnLabelNone.TabIndex = 3;
            this.btnLabelNone.Text = "[none]/[reset]";
            this.btnLabelNone.UseVisualStyleBackColor = false;
            // 
            // grpColor
            // 
            this.grpColor.Controls.Add(this.btnRed);
            this.grpColor.Controls.Add(this.btnBlue);
            this.grpColor.Controls.Add(this.btnWhite);
            this.grpColor.Controls.Add(this.btnYellow);
            this.grpColor.Controls.Add(this.btnOther);
            this.grpColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpColor.Location = new System.Drawing.Point(9, 47);
            this.grpColor.Name = "grpColor";
            this.grpColor.Size = new System.Drawing.Size(137, 348);
            this.grpColor.TabIndex = 27;
            this.grpColor.TabStop = false;
            this.grpColor.Text = "Color:";
            // 
            // grpLabel
            // 
            this.grpLabel.Controls.Add(this.btnHold);
            this.grpLabel.Controls.Add(this.btnDetonate);
            this.grpLabel.Controls.Add(this.btnLabelNone);
            this.grpLabel.Controls.Add(this.btnAbort);
            this.grpLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpLabel.Location = new System.Drawing.Point(155, 47);
            this.grpLabel.Name = "grpLabel";
            this.grpLabel.Size = new System.Drawing.Size(137, 348);
            this.grpLabel.TabIndex = 28;
            this.grpLabel.TabStop = false;
            this.grpLabel.Text = "Label:";
            // 
            // grpIndicator
            // 
            this.grpIndicator.Controls.Add(this.btnIndicatorNone);
            this.grpIndicator.Controls.Add(this.btnFRK);
            this.grpIndicator.Controls.Add(this.btnCar);
            this.grpIndicator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpIndicator.Location = new System.Drawing.Point(300, 47);
            this.grpIndicator.Name = "grpIndicator";
            this.grpIndicator.Size = new System.Drawing.Size(137, 348);
            this.grpIndicator.TabIndex = 29;
            this.grpIndicator.TabStop = false;
            this.grpIndicator.Text = "Indicator:";
            // 
            // btnIndicatorNone
            // 
            this.btnIndicatorNone.BackColor = System.Drawing.Color.Transparent;
            this.btnIndicatorNone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIndicatorNone.Location = new System.Drawing.Point(6, 149);
            this.btnIndicatorNone.Name = "btnIndicatorNone";
            this.btnIndicatorNone.Size = new System.Drawing.Size(124, 58);
            this.btnIndicatorNone.TabIndex = 2;
            this.btnIndicatorNone.Text = "[none]/[reset]";
            this.btnIndicatorNone.UseVisualStyleBackColor = false;
            // 
            // btnFRK
            // 
            this.btnFRK.BackColor = System.Drawing.Color.Transparent;
            this.btnFRK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFRK.Location = new System.Drawing.Point(6, 85);
            this.btnFRK.Name = "btnFRK";
            this.btnFRK.Size = new System.Drawing.Size(124, 58);
            this.btnFRK.TabIndex = 1;
            this.btnFRK.Text = "FRK";
            this.btnFRK.UseVisualStyleBackColor = false;
            // 
            // btnCar
            // 
            this.btnCar.BackColor = System.Drawing.Color.Transparent;
            this.btnCar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCar.Location = new System.Drawing.Point(6, 21);
            this.btnCar.Name = "btnCar";
            this.btnCar.Size = new System.Drawing.Size(124, 58);
            this.btnCar.TabIndex = 0;
            this.btnCar.Text = "CAR";
            this.btnCar.UseVisualStyleBackColor = false;
            // 
            // txtSelected
            // 
            this.txtSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSelected.Location = new System.Drawing.Point(6, 19);
            this.txtSelected.Multiline = true;
            this.txtSelected.Name = "txtSelected";
            this.txtSelected.ReadOnly = true;
            this.txtSelected.Size = new System.Drawing.Size(125, 316);
            this.txtSelected.TabIndex = 31;
            this.txtSelected.TabStop = false;
            // 
            // grpSelected
            // 
            this.grpSelected.Controls.Add(this.txtSelected);
            this.grpSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSelected.Location = new System.Drawing.Point(586, 47);
            this.grpSelected.Name = "grpSelected";
            this.grpSelected.Size = new System.Drawing.Size(137, 348);
            this.grpSelected.TabIndex = 30;
            this.grpSelected.TabStop = false;
            this.grpSelected.Text = "You selected:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(300, 16);
            this.label1.TabIndex = 31;
            this.label1.Text = "Instructions: click all that apply then review results.";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslblTimer});
            this.statusStrip1.Location = new System.Drawing.Point(0, 603);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(732, 22);
            this.statusStrip1.TabIndex = 32;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslblTimer
            // 
            this.tslblTimer.Name = "tslblTimer";
            this.tslblTimer.Size = new System.Drawing.Size(34, 17);
            this.tslblTimer.Text = "Time";
            // 
            // grpBattery
            // 
            this.grpBattery.Controls.Add(this.btnBatteryNone);
            this.grpBattery.Controls.Add(this.btnBattery4);
            this.grpBattery.Controls.Add(this.btnBattery3);
            this.grpBattery.Controls.Add(this.btnBattery2);
            this.grpBattery.Controls.Add(this.btnBattery1);
            this.grpBattery.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBattery.Location = new System.Drawing.Point(443, 47);
            this.grpBattery.Name = "grpBattery";
            this.grpBattery.Size = new System.Drawing.Size(137, 348);
            this.grpBattery.TabIndex = 30;
            this.grpBattery.TabStop = false;
            this.grpBattery.Text = "Batteries:";
            // 
            // btnBattery1
            // 
            this.btnBattery1.BackColor = System.Drawing.Color.Transparent;
            this.btnBattery1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBattery1.Location = new System.Drawing.Point(6, 21);
            this.btnBattery1.Name = "btnBattery1";
            this.btnBattery1.Size = new System.Drawing.Size(124, 58);
            this.btnBattery1.TabIndex = 0;
            this.btnBattery1.Text = "1";
            this.btnBattery1.UseVisualStyleBackColor = false;
            // 
            // btnBattery2
            // 
            this.btnBattery2.BackColor = System.Drawing.Color.Transparent;
            this.btnBattery2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBattery2.Location = new System.Drawing.Point(6, 85);
            this.btnBattery2.Name = "btnBattery2";
            this.btnBattery2.Size = new System.Drawing.Size(124, 58);
            this.btnBattery2.TabIndex = 1;
            this.btnBattery2.Text = "2";
            this.btnBattery2.UseVisualStyleBackColor = false;
            // 
            // btnBattery3
            // 
            this.btnBattery3.BackColor = System.Drawing.Color.Transparent;
            this.btnBattery3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBattery3.Location = new System.Drawing.Point(6, 149);
            this.btnBattery3.Name = "btnBattery3";
            this.btnBattery3.Size = new System.Drawing.Size(124, 58);
            this.btnBattery3.TabIndex = 2;
            this.btnBattery3.Text = "3";
            this.btnBattery3.UseVisualStyleBackColor = false;
            // 
            // btnBattery4
            // 
            this.btnBattery4.BackColor = System.Drawing.Color.Transparent;
            this.btnBattery4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBattery4.Location = new System.Drawing.Point(6, 213);
            this.btnBattery4.Name = "btnBattery4";
            this.btnBattery4.Size = new System.Drawing.Size(124, 58);
            this.btnBattery4.TabIndex = 3;
            this.btnBattery4.Text = "4";
            this.btnBattery4.UseVisualStyleBackColor = false;
            // 
            // btnBatteryNone
            // 
            this.btnBatteryNone.BackColor = System.Drawing.Color.Transparent;
            this.btnBatteryNone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBatteryNone.Location = new System.Drawing.Point(7, 277);
            this.btnBatteryNone.Name = "btnBatteryNone";
            this.btnBatteryNone.Size = new System.Drawing.Size(124, 58);
            this.btnBatteryNone.TabIndex = 4;
            this.btnBatteryNone.Text = "[none]/[reset]";
            this.btnBatteryNone.UseVisualStyleBackColor = false;
            // 
            // frmSubjectofTheButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 625);
            this.Controls.Add(this.grpBattery);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.grpSelected);
            this.Controls.Add(this.grpIndicator);
            this.Controls.Add(this.grpLabel);
            this.Controls.Add(this.grpColor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtResults);
            this.Controls.Add(this.btnReset);
            this.MaximizeBox = false;
            this.Name = "frmSubjectofTheButton";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "On the Subject of The Button";
            this.Load += new System.EventHandler(this.frmSubjectofTheButton_Load);
            this.grpColor.ResumeLayout(false);
            this.grpLabel.ResumeLayout(false);
            this.grpIndicator.ResumeLayout(false);
            this.grpSelected.ResumeLayout(false);
            this.grpSelected.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.grpBattery.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnYellow;
        private System.Windows.Forms.Button btnWhite;
        private System.Windows.Forms.Button btnBlue;
        private System.Windows.Forms.Button btnRed;
        private System.Windows.Forms.Button btnOther;
        private System.Windows.Forms.Button btnHold;
        private System.Windows.Forms.Button btnDetonate;
        private System.Windows.Forms.Button btnAbort;
        private System.Windows.Forms.TextBox txtResults;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnLabelNone;
        private System.Windows.Forms.GroupBox grpColor;
        private System.Windows.Forms.GroupBox grpLabel;
        private System.Windows.Forms.GroupBox grpIndicator;
        private System.Windows.Forms.TextBox txtSelected;
        private System.Windows.Forms.Button btnFRK;
        private System.Windows.Forms.Button btnCar;
        private System.Windows.Forms.GroupBox grpSelected;
        private System.Windows.Forms.Button btnIndicatorNone;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslblTimer;
        private System.Windows.Forms.GroupBox grpBattery;
        private System.Windows.Forms.Button btnBattery4;
        private System.Windows.Forms.Button btnBattery3;
        private System.Windows.Forms.Button btnBattery2;
        private System.Windows.Forms.Button btnBattery1;
        private System.Windows.Forms.Button btnBatteryNone;
    }
}