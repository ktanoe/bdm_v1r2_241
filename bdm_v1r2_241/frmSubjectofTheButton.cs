﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace bdm_v1r2_241
{
    public partial class frmSubjectofTheButton : Form
    {
        private readonly DateTime _openDateTime = DateTime.Now;

        private readonly string _releasingInfo =
            "Hold the button, get the strip color, then:\r\n\r\n" +
            "Blue strip: release when timer has a 4 in any position.\r\n" +
            "Yellow strip: release when timer has a 5 in any position.\r\n" +
            "Any other: release when timer has a 1 in any position";

        private ButtonData _buttonData;

        public frmSubjectofTheButton()
        {
            InitializeComponent();
        }

        private void frmSubjectofTheButton_Load(object sender, EventArgs e)
        {
            // we want each button except for reset to fire the same event
            foreach (var btn in Controls.OfType<GroupBox>().SelectMany(grpCtrl => grpCtrl.Controls.OfType<Button>()))
            {
                Debug.WriteLine(btn.Name);
                btn.Click += ClickedAButton;
            }

            Reset();
        }

        private void Reset()
        {
            _buttonData = new ButtonData();
            txtSelected.Text = string.Empty;
            txtResults.Text = string.Empty;
        }

        private void RunTheButton()
        {
            if (_buttonData.Color == ButtonColor.Yellow)
            {
                txtResults.Text = _releasingInfo;
                return;
            }

            if (_buttonData.Indicator == ButtonIndicator.Frk &&
                (_buttonData.Battery == ButtonBattery.Three || _buttonData.Battery == ButtonBattery.Four))
            {
                txtResults.Text = "Press and immediately release the button.";
                return;
            }

            if (_buttonData.Label == ButtonLabel.Detonate &&
                (_buttonData.Battery == ButtonBattery.Two || _buttonData.Battery == ButtonBattery.Three ||
                 _buttonData.Battery == ButtonBattery.Four))
            {
                txtResults.Text = "Press and immediately release the button.";
                return;
            }

            if (_buttonData.Color == ButtonColor.Red && _buttonData.Label == ButtonLabel.Hold)
            {
                txtResults.Text = "Press and immediately release the button.";
                return;
            }

            if (_buttonData.Color == ButtonColor.Blue && _buttonData.Label == ButtonLabel.Abort)
            {
                txtResults.Text = _releasingInfo;
                return;
            }

            if (_buttonData.Color == ButtonColor.White && _buttonData.Indicator == ButtonIndicator.Car)
            {
                txtResults.Text = _releasingInfo;
                return;
            }

            txtResults.Text = _releasingInfo;
        }

        private void ClickedAButton(object sender, EventArgs e)
        {
            var btn = (Button) sender;

            switch (btn.Text)
            {
                case "RED":
                    _buttonData.Color = ButtonColor.Red;
                    break;
                case "BLUE":
                    _buttonData.Color = ButtonColor.Blue;
                    break;
                case "YELLOW":
                    _buttonData.Color = ButtonColor.Yellow;
                    break;
                case "WHITE":
                    _buttonData.Color = ButtonColor.White;
                    break;
                case "OTHER":
                    _buttonData.Color = ButtonColor.Other;
                    break;
                case "Hold":
                    _buttonData.Label = ButtonLabel.Hold;
                    break;
                case "Abort":
                    _buttonData.Label = ButtonLabel.Abort;
                    break;
                case "Detonate":
                    _buttonData.Label = ButtonLabel.Detonate;
                    break;
                case "CAR":
                    _buttonData.Indicator = ButtonIndicator.Car;
                    break;
                case "FRK":
                    _buttonData.Indicator = ButtonIndicator.Frk;
                    break;
                case "1":
                    _buttonData.Battery = ButtonBattery.One;
                    break;
                case "2":
                    _buttonData.Battery = ButtonBattery.Two;
                    break;
                case "3":
                    _buttonData.Battery = ButtonBattery.Three;
                    break;
                case "4":
                    _buttonData.Battery = ButtonBattery.Four;
                    break;
            }

            switch (btn.Name)
            {
                case "btnLabelNone":
                    _buttonData.Label = ButtonLabel.None;
                    break;
                case "btnIndicatorNone":
                    _buttonData.Indicator = ButtonIndicator.None;
                    break;
                case "btnBatteryNone":
                    _buttonData.Battery = ButtonBattery.None;
                    break;
            }

            txtSelected.Text =
                $"{_buttonData.Color}\r\n{_buttonData.Label}\r\n{_buttonData.Indicator}\r\n{_buttonData.Battery}";

            RunTheButton();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tslblTimer.Text = $@"{DateTime.Now.Subtract(_openDateTime):dd\.hh\:mm\:ss}";
        }

        #region "Class & Enums"

        private class ButtonData
        {
            public ButtonBattery Battery;
            public ButtonColor Color;
            public ButtonIndicator Indicator;
            public ButtonLabel Label;

            public ButtonData()
            {
                Color = ButtonColor.Other;
                Indicator = ButtonIndicator.None;
                Label = ButtonLabel.None;
                Battery = ButtonBattery.None;
            }
        }

        private enum ButtonColor
        {
            Red,
            Blue,
            Yellow,
            White,
            Other
        }

        private enum ButtonLabel
        {
            Hold,
            Abort,
            Detonate,
            None
        }

        private enum ButtonIndicator
        {
            Car,
            Frk,
            None
        }

        private enum ButtonBattery
        {
            One,
            Two,
            Three,
            Four,
            None
        }

        #endregion
    }
}