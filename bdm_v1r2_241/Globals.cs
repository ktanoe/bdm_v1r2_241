﻿namespace bdm_v1r2_241
{
    public enum SerialVowel
    {
        Vowel,
        NoVowel
    }

    public enum SerialOddEven
    {
        Odd,
        Even
    }

    public static class Globals
    {
        public static int Batteries = 0;
        public static SerialOddEven SerialOddEven = SerialOddEven.Even;
        public static SerialVowel SerialVowel = SerialVowel.Vowel;
        public static int ParallelPorts = 0;



        static Globals()
        {
        }
    }
}