﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace bdm_v1r2_241
{
    public partial class frmSubjectofMemory : Form
    {
        private readonly List<string> _acceptableEntriesList = new List<string> {"1", "2", "3", "4"};

        private readonly DateTime _openDateTime = DateTime.Now;
        private int _stageCount;
        private Stage _stageFive;
        private Stage _stageFour;
        private Stage _stageOne;
        private Stage _stageThree;
        private Stage _stageTwo;

        public frmSubjectofMemory()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tslblTimer.Text = $@"{DateTime.Now.Subtract(_openDateTime):dd\.hh\:mm\:ss}";
        }

        private void txtEntry_TextChanged(object sender, EventArgs e)
        {
            if (txtEntry.Text.Trim().Length != 5) return;

            if (_stageCount == 1)
            {
                _stageOne = LoadStage(txtEntry.Text.Trim());
            }

        }

        private Stage LoadStage(string entry)
        {
            var stage = new Stage
            {
                FirstPos = entry[0].ToString(),
                SecondPos = entry[1].ToString(),
                ThirdPos = entry[2].ToString(),
                FourthPos = entry[3].ToString(),
                Display = entry[4].ToString()
            };
            return stage;
        }

        private void txtEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            // only allow the four keys we want
            if (!_acceptableEntriesList.Contains(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void txtEntry_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {

        }

        private void frmSubjectofMemory_Load(object sender, EventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            _stageCount = 1;
            _stageOne = new Stage();
            _stageTwo = new Stage();
            _stageThree = new Stage();
            _stageFour = new Stage();
            _stageFive = new Stage();
            txtTracking.Text = string.Empty;
            txtEntry.Text = string.Empty;
        }

        private class Stage
        {
            public string ButtonLabel = string.Empty;
            public string ButtonPosition = string.Empty;
            public string Display = string.Empty;
            public string FirstPos = string.Empty;
            public string FourthPos = string.Empty;
            public string SecondPos = string.Empty;
            public string ThirdPos = string.Empty;
        }


    }
}