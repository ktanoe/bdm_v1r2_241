﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace bdm_v1r2_241
{
    public partial class frmSubjectofWireSequences : Form
    {
        private readonly List<string> _acceptableColorEntries = new List<string> {"r", "R", "b", "B", "k", "K"};
        private readonly List<string> _acceptableLetterEntries = new List<string> {"a", "A", "b", "B", "c", "C"};
        private readonly DateTime _openDateTime = DateTime.Now;
        private List<Wire> _doneList;
        private List<Wire> _wiresList;

        public frmSubjectofWireSequences()
        {
            InitializeComponent();
        }

        private void frmSubjectofWireSequences_Load(object sender, EventArgs e)
        {
            Reset();
        }

        private void txtEntry_TextChanged(object sender, EventArgs e)
        {
            if (txtEntry.Text.Trim().Length != 2) return;


            var color = txtEntry.Text[0].ToString().ToUpper();
            var letter = txtEntry.Text[1].ToString().ToUpper();

            if (!_acceptableColorEntries.Contains(color) || !_acceptableLetterEntries.Contains(letter))
            {
                lblAction.Text = "Invalid combination; try again.";
                txtEntry.Clear();
                return;
            }

            var wire =
                _wiresList.Where(x => x.WireColor.Equals(color, StringComparison.CurrentCultureIgnoreCase))
                    .Where(x => x.Completed == false)
                    .OrderBy(x => x.Occurrence)
                    .FirstOrDefault();

            if (wire == null)
            {
                lblAction.Text = "Invalid combination; try again.";
                txtEntry.Clear();
                return;
            }

            wire.Completed = true;
            wire.CompletedDateTime = DateTime.Now;

            _doneList.Add(wire);

            // show the user
            UpdateOccurrenceList(_doneList);

            if (wire.CutLetters.Contains(letter))
            {
                lblAction.ForeColor = Color.Red;
                lblAction.Text = $"CUT: {wire.WireColorFullName.ToLower()}-{letter.ToLower()}";
            }
            else
            {
                lblAction.ForeColor = Color.Black;
                lblAction.Text = $"DO NOT cut: {wire.WireColorFullName.ToLower()}-{letter.ToLower()}";
            }

            txtEntry.Clear();
        }

        private void UpdateOccurrenceList(List<Wire> buildList)
        {
            txtTracking.Text = buildList.Aggregate(string.Empty,
                (current, wire) => $"{wire.Occurrence}-{wire.WireColorFullName}\r\n" + current);
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            // bail if we have no list
            if (_doneList.Count == 0)
            {
                lblAction.ForeColor = Color.Black;
                lblAction.Text = "Nothing to undo.";
                txtEntry.Focus();
                return;
            }
            // get the last wire added
            var doneWire = _doneList.OrderByDescending(x => x.CompletedDateTime).FirstOrDefault();
            // remove wire from donelist
            _doneList.Remove(doneWire);
            // find that wire in our big list, using unique key
            var wire = _wiresList.FirstOrDefault(x => doneWire != null && x.UniqueKey == doneWire.UniqueKey);
            // set our big list wire to not have been completed
            if (wire != null)
            {
                wire.Completed = false;
                wire.CompletedDateTime = DateTime.MinValue;
            }

            // update what the user sees
            UpdateOccurrenceList(_doneList);
            lblAction.Text = string.Empty;
            // back to quick entry
            txtEntry.Focus();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tslblTimer.Text = $@"{DateTime.Now.Subtract(_openDateTime):dd\.hh\:mm\:ss}";
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            _doneList = new List<Wire>();

            _wiresList = new List<Wire>
            {
                new Wire("R", "RED", 1, "C"),
                new Wire("R", "RED", 2, "B"),
                new Wire("R", "RED", 3, "A"),
                new Wire("R", "RED", 4, "AC"),
                new Wire("R", "RED", 5, "B"),
                new Wire("R", "RED", 6, "AC"),
                new Wire("R", "RED", 7, "ABC"),
                new Wire("R", "RED", 8, "AB"),
                new Wire("R", "RED", 9, "B"),
                new Wire("B", "BLUE", 1, "B"),
                new Wire("B", "BLUE", 2, "AC"),
                new Wire("B", "BLUE", 3, "B"),
                new Wire("B", "BLUE", 4, "A"),
                new Wire("B", "BLUE", 5, "B"),
                new Wire("B", "BLUE", 6, "BC"),
                new Wire("B", "BLUE", 7, "C"),
                new Wire("B", "BLUE", 8, "AC"),
                new Wire("B", "BLUE", 9, "A"),
                new Wire("K", "BLACK", 1, "ABC"),
                new Wire("K", "BLACK", 2, "AC"),
                new Wire("K", "BLACK", 3, "B"),
                new Wire("K", "BLACK", 4, "AC"),
                new Wire("K", "BLACK", 5, "B"),
                new Wire("K", "BLACK", 6, "BC"),
                new Wire("K", "BLACK", 7, "AB"),
                new Wire("K", "BLACK", 8, "C"),
                new Wire("K", "BLACK", 9, "C")
            };

            txtEntry.Clear();
            txtTracking.Clear();
            txtEntry.Focus();
        }

        private void txtEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            // only allow the six keys we want
            if (!_acceptableColorEntries.Contains(e.KeyChar.ToString()) &&
                !_acceptableLetterEntries.Contains(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private class Wire
        {
            public readonly string CutLetters;
            public readonly int Occurrence;
            public readonly Guid UniqueKey;
            public readonly string WireColor;
            public readonly string WireColorFullName;
            public bool Completed;
            public DateTime CompletedDateTime;

            public Wire(string wireColor, string wireColorFullName, int occurrence, string cutLetters)
            {
                UniqueKey = Guid.NewGuid();
                WireColor = wireColor;
                WireColorFullName = wireColorFullName;
                Occurrence = occurrence;
                CutLetters = cutLetters;
                Completed = false;
                //CompletedDateTime;
            }
        }
    }
}