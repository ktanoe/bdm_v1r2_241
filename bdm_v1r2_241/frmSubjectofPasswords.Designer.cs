﻿namespace bdm_v1r2_241
{
    partial class frmSubjectofPasswords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.Col1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Passwords = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Col2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Col3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Col4 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Col5 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslblTimer = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "First (left)";
            // 
            // Col1
            // 
            this.Col1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Col1.Location = new System.Drawing.Point(80, 41);
            this.Col1.MaxLength = 6;
            this.Col1.Name = "Col1";
            this.Col1.Size = new System.Drawing.Size(85, 22);
            this.Col1.TabIndex = 0;
            this.Col1.WordWrap = false;
            this.Col1.TextChanged += new System.EventHandler(this.CalcPass);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(423, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Instructions: enter in ALL letters for each column until one word remains.";
            // 
            // Passwords
            // 
            this.Passwords.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Passwords.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Passwords.Location = new System.Drawing.Point(13, 206);
            this.Passwords.Multiline = true;
            this.Passwords.Name = "Passwords";
            this.Passwords.ReadOnly = true;
            this.Passwords.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.Passwords.Size = new System.Drawing.Size(420, 111);
            this.Passwords.TabIndex = 3;
            this.Passwords.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 187);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Results";
            // 
            // Col2
            // 
            this.Col2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Col2.Location = new System.Drawing.Point(80, 67);
            this.Col2.MaxLength = 6;
            this.Col2.Name = "Col2";
            this.Col2.Size = new System.Drawing.Size(85, 22);
            this.Col2.TabIndex = 1;
            this.Col2.WordWrap = false;
            this.Col2.TextChanged += new System.EventHandler(this.CalcPass);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Second";
            // 
            // Col3
            // 
            this.Col3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Col3.Location = new System.Drawing.Point(80, 93);
            this.Col3.MaxLength = 6;
            this.Col3.Name = "Col3";
            this.Col3.Size = new System.Drawing.Size(85, 22);
            this.Col3.TabIndex = 2;
            this.Col3.WordWrap = false;
            this.Col3.TextChanged += new System.EventHandler(this.CalcPass);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "Third";
            // 
            // Col4
            // 
            this.Col4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Col4.Location = new System.Drawing.Point(80, 121);
            this.Col4.MaxLength = 6;
            this.Col4.Name = "Col4";
            this.Col4.Size = new System.Drawing.Size(85, 22);
            this.Col4.TabIndex = 3;
            this.Col4.WordWrap = false;
            this.Col4.TextChanged += new System.EventHandler(this.CalcPass);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Fourth";
            // 
            // Col5
            // 
            this.Col5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Col5.Location = new System.Drawing.Point(80, 149);
            this.Col5.MaxLength = 6;
            this.Col5.Name = "Col5";
            this.Col5.Size = new System.Drawing.Size(85, 22);
            this.Col5.TabIndex = 4;
            this.Col5.WordWrap = false;
            this.Col5.TextChanged += new System.EventHandler(this.CalcPass);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(13, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 16);
            this.label7.TabIndex = 11;
            this.label7.Text = "Fifth";
            // 
            // btnClearAll
            // 
            this.btnClearAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearAll.Location = new System.Drawing.Point(13, 363);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(100, 45);
            this.btnClearAll.TabIndex = 12;
            this.btnClearAll.TabStop = false;
            this.btnClearAll.Text = "R&eset";
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslblTimer});
            this.statusStrip1.Location = new System.Drawing.Point(0, 418);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(444, 22);
            this.statusStrip1.TabIndex = 13;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslblTimer
            // 
            this.tslblTimer.Name = "tslblTimer";
            this.tslblTimer.Size = new System.Drawing.Size(34, 17);
            this.tslblTimer.Text = "Time";
            // 
            // frmPasswords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 440);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnClearAll);
            this.Controls.Add(this.Col5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Col4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Col3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Col2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Passwords);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Col1);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "frmPasswords";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "On the subject of Passwords";
            this.Load += new System.EventHandler(this.frmPassword_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Col1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Passwords;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Col2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Col3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Col4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Col5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnClearAll;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslblTimer;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}