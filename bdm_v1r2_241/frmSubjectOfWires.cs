﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace bdm_v1r2_241
{
    public partial class frmSubjectOfWires : Form
    {
        private readonly DateTime _openDateTime = DateTime.Now;
        private SerialOddEvenNone _serialOddEvenNone;

        private List<string> _wires;

        public frmSubjectOfWires()
        {
            InitializeComponent();
        }

        private void frmSubjectOfWires_Load(object sender, EventArgs e)
        {
            // set up event handling for all color buttons
            btnRed.Click += AddTheWires;
            btnBlue.Click += AddTheWires;
            btnWhite.Click += AddTheWires;
            btnBlack.Click += AddTheWires;
            btnYellow.Click += AddTheWires;

            Reset();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tslblTimer.Text = $@"{DateTime.Now.Subtract(_openDateTime):dd\.hh\:mm\:ss}";
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void rdOdd_Click(object sender, EventArgs e)
        {
            _serialOddEvenNone = SerialOddEvenNone.Odd;
            RunTheSolution();
        }

        private void rdEven_Click(object sender, EventArgs e)
        {
            _serialOddEvenNone = SerialOddEvenNone.Even;
            RunTheSolution();
        }

        private void Reset()
        {
            _serialOddEvenNone = SerialOddEvenNone.None;
            txtResults.Clear();
            txtSelected.Clear();
            rdEven.Checked = false;
            rdOdd.Checked = false;
            grpSerial.Enabled = false;
            _wires = new List<string>();
        }

        private void AddTheWires(object sender, EventArgs e)
        {
            if (_wires.Count > 5)
            {
                txtResults.Text = "You already have 6 wires! Hit reset to try again.";
                return;
            }

            var btnPressed = (Button) sender;
            _wires.Add(btnPressed.Text);

            txtSelected.Text += btnPressed.Text + "\r\n";

            RunTheSolution();
        }

        private void RunTheSolution()
        {
            txtResults.Clear();

            if (_wires.Count < 3) return;

            switch (_wires.Count)
            {
                case 3:
                    RunThreeWires();
                    break;
                case 4:
                    RunFourWires();
                    break;
                case 5:
                    RunFiveWires();
                    break;
                case 6:
                    RunSixWires();
                    break;
            }
        }

        private void RunThreeWires()
        {
            if (!_wires.Contains("RED"))
            {
                txtResults.Text = "Cut the second wire.";
                return;
            }

            if (_wires[2].Equals("WHITE", StringComparison.OrdinalIgnoreCase))
            {
                txtResults.Text = "Cut the last wire.";
                return;
            }

            if (_wires.Count(x => x.Equals("BLUE", StringComparison.OrdinalIgnoreCase)) > 1)
            {
                txtResults.Text = "Cut the last blue wire.";
                return;
            }

            txtResults.Text = "Cut the last wire.";

        }

        private void RunFourWires()
        {
            if (_wires.Count(x => x.Equals("RED", StringComparison.OrdinalIgnoreCase)) > 1)
            {
                if (_serialOddEvenNone == SerialOddEvenNone.None)
                {
                    txtResults.Text = "CHOOSE SERIAL NUMBER ODD OR EVEN!";
                    grpSerial.Enabled = true;
                    return;
                }
                if (_serialOddEvenNone == SerialOddEvenNone.Odd)
                {
                    txtResults.Text = "Cut the last red wire.";
                    return;
                }
            }

            if (_wires[3].Equals("YELLOW", StringComparison.OrdinalIgnoreCase) && !_wires.Contains("RED"))
            {
                txtResults.Text = "Cut the first wire.";
                return;
            }

            if (_wires.Count(x => x.Equals("BLUE", StringComparison.OrdinalIgnoreCase)) == 1)
            {
                txtResults.Text = "Cut the first wire.";
                return;
            }

            if (_wires.Count(x => x.Equals("YELLOW", StringComparison.OrdinalIgnoreCase)) > 1)
            {
                txtResults.Text = "Cut the last wire.";
                return;
            }

            txtResults.Text = "Cut the second wire.";
        }

        private void RunFiveWires()
        {
            if (_wires[4].Equals("BLACK", StringComparison.OrdinalIgnoreCase))
            {
                if (_serialOddEvenNone == SerialOddEvenNone.None)
                {
                    txtResults.Text = "CHOOSE SERIAL NUMBER ODD OR EVEN!";
                    grpSerial.Enabled = true;
                    return;
                }
                if (_serialOddEvenNone == SerialOddEvenNone.Odd)
                {
                    txtResults.Text = "Cut the fourth wire.";
                    return;
                }
            }

            if (_wires.Count(x => x.Equals("RED", StringComparison.OrdinalIgnoreCase)) == 1 &&
                _wires.Count(x => x.Equals("YELLOW", StringComparison.OrdinalIgnoreCase)) > 1)
            {
                txtResults.Text = "Cut the first wire.";
                return;
            }

            if (!_wires.Contains("BLACK"))
            {
                txtResults.Text = "Cut the second wire.";
                return;
            }

            txtResults.Text = "Cut the first wire.";
        }

        private void RunSixWires()
        {
            if (!_wires.Contains("YELLOW"))
            {
                if (_serialOddEvenNone == SerialOddEvenNone.None)
                {
                    txtResults.Text = "CHOOSE SERIAL NUMBER ODD OR EVEN!";
                    grpSerial.Enabled = true;
                    return;
                }
                if (_serialOddEvenNone == SerialOddEvenNone.Odd)
                {
                    txtResults.Text = "Cut the third wire.";
                    return;
                }
            }

            if (_wires.Count(x => x.Equals("YELLOW", StringComparison.OrdinalIgnoreCase)) == 1 &&
                _wires.Count(x => x.Equals("WHITE", StringComparison.OrdinalIgnoreCase)) > 1)
            {
                txtResults.Text = "Cut the fourth wire.";
                return;
            }

            if (!_wires.Contains("RED"))
            {
                txtResults.Text = "Cut the last wire.";
                return;
            }

            txtResults.Text = "Cut the fourth wire.";
        }

        private enum SerialOddEvenNone
        {
            None,
            Odd,
            Even
        }
    }
}